<?php

namespace Src\controllers;

use Src\helpers\Helpers;
use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($data) {

		try {
			$clientObject = new Client;

			//validate the fields
			if ($data['client']['phone']) {
				$validPhone = $clientObject->validatePhone($data['client']['phone']);
			} 

			if ($data['client']['email']) {
				$validEmail = filter_var($data['client']['phone'], FILTER_VALIDATE_EMAIL);
			} 

			$helpers = new Helpers;

			$clientExist = $helpers->arraySearchI($data['client']['email'], $clientObject->getClients(), 'email');

			if(!$clientExist) {
				$newClient = $clientObject->createClient($data['client']);
				$data['client']['id'] = $newClient['id'];
			}
			else {
				echo "Client alreadys exists";
			}
			
			/* if ($validEmail && !$clientExist) */
				return $this->getBookingModel()->createBooking($data);

		} catch (\Throwable $th) {
			throw $th;
		}

	}
}