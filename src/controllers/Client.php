<?php

namespace Src\controllers;

use Src\helpers\Helpers;
use Src\models\ClientModel;

class Client {

	private function getClientModel(): ClientModel {
		return new ClientModel();
	}

	public function getClients() {
		return $this->getClientModel()->getClients();
	}

	public function createClient($client) {
		return $this->getClientModel()->createClient($client);
	}

	public function updateClient($client) {
		return $this->getClientModel()->updateClient($client);
	}

	public function getClientById($id) {
		return $this->getClientModel()->getClientById($id);
	}

	public function validatePhone($phone) {
		$access_key = 'e6da9effc74ca6834920c5accf50853c';

		// set phone number
		$phone = '14158586273';

		// Initialize CURL:
		$ch = curl_init('http://apilayer.net/api/validate?access_key='.$access_key.'&number='.$phone.'');  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Store the data:
		$json = curl_exec($ch);
		curl_close($ch);

		// Decode JSON response:
		$validationResult = json_decode($json, true);

		// Access and use your preferred validation result objects
		return $validationResult['valid'];
	}
}