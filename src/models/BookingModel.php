<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;


	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	// I found a bug here, this method was using a static 
	public function getBookings() {
		return $this->bookingData;
	}
	
	public function createBooking($data) {

		$booking = $this->getBookings();

		$data['id'] = end($booking)['id'] + 1;
		$booking[] = $data;

		$this->helper->putJson($booking, 'bookings');

		return $data;
	}
}